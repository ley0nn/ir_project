#!/usr/bin/python3
# Leon Graumans - Luuk Looijenga - Mike Zhang
# Information Retrieval project 28-01-2018

# COMMENT: File looks a bit messy, but to test other features some lines are commented. 

import os
import io
import string
import pickle
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.corpus import alpino as alp
from nltk.tag import UnigramTagger, BigramTagger

training_corpus = alp.tagged_sents()
unitagger = UnigramTagger(training_corpus)
bitagger = BigramTagger(training_corpus, backoff=unitagger)

pos_tag = bitagger.tag

''' Three functions of featx.py, made by A. Toral, edited by Luuk. '''
def bag_of_words(words):
    #tagged_words = pos_tag(words)
    return dict([(word, 1) for word in words])
    #return dict([(word[0] + " " + str(word[1]), 1) for word in tagged_words])

def bag_of_words_not_in_set(words, badwords):
    return bag_of_words(set(words) - set(badwords))

def bag_of_non_stopwords(words, stopfile='dutch'):
    badwords = ['propname']
    #badwords = stopwords.words(stopfile)
    #badwords.append("*propname*")
    #return bag_of_words_not_in_set(words, badwords)
    new_words = [word for word in words if word not in badwords]
    return bag_of_words(new_words)

def find_bigrams(input_list, stopfile='dutch'):
    badwords = ['PROPNAME']
    #badwords = stopwords.words(stopfile)
    #badwords.append("*PROPNAME*")
    new_words = [word for word in input_list if word not in badwords]
    # tagged_words = pos_tag(new_words)

    bigram_list = []
    for i in range(len(new_words)-1):
        bigram_list.append(new_words[i] + " " + str(new_words[i+1]))

    # bigram_list = []
    # for i in range(len(tagged_words)-1):
    #     bigram_list.append(tagged_words[i][0] + " " + str(tagged_words[i][1]) \
    #     + " " + tagged_words[i+1][0] + " " +  str(tagged_words[i+1][1]))
    return bigram_list

def main():
    directory = "csicorpus/reviews_propname/"
    labels = []
    unigrams = []
    bigrams = []
    uni_bi = []
    # badwords = ['*PROPNAME*']
    badwords = stopwords.words('dutch')
    badwords.append("propname")
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            with open(directory + "/" + filename, "r", encoding='utf-8') as f:
                for line in f:
                    tokenizer = RegexpTokenizer(r'\w+')
                    #tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
                    tokens = tokenizer.tokenize(line)

                    bigram_tokens = find_bigrams(tokens, stopfile='dutch')
                    bigram_tokens = [x.lower() for x in bigram_tokens]

                    bigram_tokens = bag_of_words(bigram_tokens) # Deze zijn dus nog niet gefilterd op stopwoorden en punc

                    # lowercase and remove punc and Dutch stopwords
                    tokens = [x.lower() for x in tokens]
                    # tokens = bag_of_words_not_in_set(tokens, string.punctuation)
                    tokens = bag_of_non_stopwords(tokens, stopfile="dutch")
            f.close()

            # temp_bigram = dict()

            # for word in badwords:
            #     for i in bigram_tokens.keys():
            #         if word in i:
            #             temp_bigram[i] = 1
            #         else:
            #             continue

            # bigrams_f = dict(set(bigram_tokens.items()) - set(temp_bigram.items()))
            merged_dict = {**bigram_tokens, **tokens}


            unigrams.append(tokens)
            bigrams.append(bigram_tokens)
            uni_bi.append(merged_dict)
            # bigrams.append(bigrams_f)


            if filename.endswith("Neg.txt"):
                labels.append("N")
                continue
            elif filename.endswith("Pos.txt"):
                labels.append("P")
                continue
        else:
            continue


    with open('labels.pickle', 'wb') as f:
        pickle.dump(labels, f)

    with open('bigrams.pickle', 'wb') as f:
        pickle.dump(bigrams, f)

    with open('unigrams.pickle', 'wb') as f:
        pickle.dump(unigrams, f)

    with open('unibigram.pickle', 'wb') as f:
        pickle.dump(uni_bi, f)

main()
